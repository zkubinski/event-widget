#include <QApplication>

#include "parentwidget.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    ParentWidget pw;
    pw.show();

    return a.exec();
}
