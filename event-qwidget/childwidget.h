#ifndef CHILDWIDGET_H
#define CHILDWIDGET_H

#include <QObject>
#include <QWidget>
#include <QEvent>
#include <QMouseEvent>

class ChildWidget : public QWidget
{
    Q_OBJECT

public:
    ChildWidget(QWidget *parent=nullptr);

    // QWidget interface
protected:
    virtual void mouseDoubleClickEvent(QMouseEvent *event) override;
    // QWidget interface
protected:
    virtual void resizeEvent(QResizeEvent *event) override;

signals:
    void mouseDoublePressed(QMouseEvent *event);
};
#endif // CHILDWIDGET_H
