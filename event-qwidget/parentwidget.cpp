#include "parentwidget.h"

ParentWidget::ParentWidget(QWidget *parent) : QWidget(parent)
{
    child = new ChildWidget(this);
    QObject::connect(child, &ChildWidget::mouseDoublePressed, this, &ParentWidget::handleMouseDoublePress);
}

void ParentWidget::handleMouseDoublePress(QMouseEvent *event)
{
    if(event->type()==QEvent::MouseButtonDblClick){
        qInfo()<< "double click";
    }
}
