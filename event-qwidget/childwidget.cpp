#include "childwidget.h"

ChildWidget::ChildWidget(QWidget *parent) : QWidget(parent)
{
}

void ChildWidget::mouseDoubleClickEvent(QMouseEvent *event)
{
    emit mouseDoublePressed(event);
    QWidget::mouseDoubleClickEvent(event);
}

void ChildWidget::resizeEvent(QResizeEvent *event)
{
    QWidget::resizeEvent(event);
}
