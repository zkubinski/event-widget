#ifndef PARENTWIDGET_H
#define PARENTWIDGET_H

#include <QObject>
#include <QWidget>

#include "childwidget.h"

class ParentWidget : public QWidget
{
    Q_OBJECT

public:
    ParentWidget(QWidget *parent=nullptr);

private:
    ChildWidget *child;


private slots:
    void handleMouseDoublePress(QMouseEvent *event);
};

#endif // PARENTWIDGET_H
